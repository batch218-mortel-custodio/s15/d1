console.log("Hello world!");
// enclose with quotation mark string data
console.log("Hello world!");

console.
log
(
		"Hello, everyone!"
)

// ; delimeter
// we use delimeter to end our code

// used for single line comment (ctrl / )

/* multi line comment (ctrl shift / ) */

// Syntax and statement

// Statements in programming are instructions that we tell to computer to perform (sentence)

// syntax in programming ,it is the set of rules that describes how statements must be considered (grammar)

// variables
/* it is used to contain data 

	Syntax in declaring variables:

	-let/const variableName
*/

let myVariable = "Hello";
console.log(myVariable);


// console.log(hello); // will result to not defined error

/*
	Guides in writing variables:

	1. Use the "let" keyword followed by the variable name of your choosing and use the assginment operator (=) to assign a value
	2. Variable  names should start with a lowercase character, use camelCase for multiple words.
	3. For constant variables, use the "const" keyword
	4. Variable names should be indicative or descriptive of the valuue being stored 
	5. Never name a variable starting with numbers


*/

let productName= 'desktop computer';
console.log(productName);

let product = "Alvin's computer";
console.log(product);

let productPrice = 18999;
console.log(productPrice);

const interest = 3.539;
console.log(interest);

console.log(interest*productPrice);

productName = "laptop";
console.log(productName);

let friend = "kate";
friend = "Jane";
console.log(friend);

const pi = 3.14;

let supplier; // declaration
supplier = "John Smith Trading" // initializing
console.log(supplier); // printing

supplier = "Zuitt Store";
console.log(supplier);

// Multiple variable declaration
let productCode = "DC017";
const productBrand = "Dell";
console.log(productCode, productBrand);

// Using a variable with a reserved keyword
// do not use "let" or other syntax as a variable name

// [SECTION] Data Types

// Strings
//  Strings are series of characters that create a word, phrase, sentence, or anything related to creating text.

// Strings in javascript is enclosed with single quotation mark or double quotation mark.


let country = 'Philippines';
let province = 'Metro Manila';

console.log(province + ', ' + country);

let fullAddress = province + ', ' + country;
console.log(fullAddress);

console.log("Philippines" + ', ' + "Metro Manila");

// Escape character (\)
// "\n" refers to creating a new line or set text to next line;

console.log("line1\nline2");

let mailAddress = "Metro Manila\nPhilippines";
console.log(mailAddress);

let message = "John's employee went home early."
console.log(message);

message = 'John\'s employee went home early';
console.log(message);

// Numbers
let  headcount = 26;
console.log(headcount);

// Decimal / float
let grade = 98.7;
console.log(grade);

//Exponential Notation 20,000,000,000
let planetDistance = 2e10;
console.log(planetDistance);

console.log("John's grade last quarter is "+ grade);

// Arrays
// it is to store multiple values with similar data type
let grades = [98.7, 95.4, 90.2, 94.65];
console.log(grades);


// different data types
// storign different data types inside an array is not recommended because it will not make sense in the context of programming
let details  = ["John", "Smith", 32, true];
console.log(details);


// Objects
// Object are another special kind of data type that is used to mimic real world objects/items.

// Syntax:
/*

	let/const objectName = {
		propertyA: value,
		propertyB: value
	}
*/


let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	isMarried: false,
	contact: ["0912 345 6789", "8123 7444"],
/*	address: {
		houseNumber: "345",
		city: "Manila"
	}*/
};
console.log(person);

const myGrades = {
	firstGrading: 98,
	secondGrading: 98,
	thirdGrading: 98,
	fourthGrading: 98,
};

console.log(myGrades);

let stringValue = "string";
let numberValue = 10;
let booleanValue = true;
let waterBills = [1000, 640, 700];
// myGrades as object 

console.log(typeof stringValue);

console.log(typeof numberValue);

console.log(typeof booleanValue);

// Constant(cons) objects and arrays
// we cannot reassign the value of the variable

const anime = ["one piece", "code geas", "monster", "demon slayer"];

//  index - is the position of the element starting zero.

anime[0] = "naruto";

console.log(anime);

//Null - it is used to intentionally express the absence of a value
let spouse = null;
console.log(spouse);


//Undefined - represents the state of a variable that has been declared but without an assigned value


let fullName;
console.log(fullName);



